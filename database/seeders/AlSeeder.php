<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\al;

class AlSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        al::create([
            'nama' => 'affan',
            'alamat' => 'katapang',
            'asalsekolah' => 'ma omben',
            'jurusan' => 'agama',   
        ]);
    }
}
