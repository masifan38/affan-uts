<?php

namespace App\Http\Controllers;

use App\Models\al;
use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;

class AlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alldata = Data::all();
        return view('semuadata', compact('alldata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('tambahdata');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        data::create([
            'nama'=>$request->nama,
            'job'=>$request->job,
        ]);
        return redirect('tampilan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\al  $al
     * @return \Illuminate\Http\Response
     */
    public function show(Data @id)
    {
        $satudata = Data::find($id);
        return view('ettok', compact('satudata'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\al  $al
     * @return \Illuminate\Http\Response
     */
    public function edit(al $al)
    {
        $editing = data::findorfail($id);
        return view('kampedit',compact('editing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\al  $al
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, al $al)
    {
        $editing = data::findorfail($id);
        $editing->update($request->all());

        return redirect('tampil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\al  $la
     * @return \Illuminate\Http\Response
     */
    public function destroy(data $data, $id)
    {
        $editing=data::findorfail($id);
        $editing->delete();

        return back();
    }
}
