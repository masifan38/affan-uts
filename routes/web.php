<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('conten');
});
Route::get('/table', function () {
    return view('tabel-data');
});

Route::get('tampil', [datacontroler::class,'index']);

Route::get('tampil/{id}', [datacontroler::class,'show']);

Route::get('buatdata', [datacontroler::class,'icreate']);
Route::post('simpandata', [datacontroler::class,'store']);

Route::get('editdata/{id}', [datacontroler::class,'edit']);
Route::get('update/{id}', [datacontroler::class,'iupdate']);

Route::get('delete/{id}', [datacontroler::class,'destroy']);